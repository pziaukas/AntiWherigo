import unittest
import anti_wherigo


class AntiWherigoTest(unittest.TestCase):
    def test_version(self) -> None:
        self.assertEqual(anti_wherigo.__version__, '0.4.0')


if __name__ == '__main__':
    unittest.main()
