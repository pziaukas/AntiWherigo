.PHONY: environment lint release test
$(VERBOSE).SILENT:

#################################################################################
# GLOBALS                                                                       #
#################################################################################

EXECUTOR = poetry run

#################################################################################
# COMMANDS                                                                      #
#################################################################################

## Install (or update) Python environment
environment:
	mkdir -p data &&\
	poetry install

## Inspect the code style using flake8 and pydocstyle
lint:
	$(EXECUTOR) flake8 --max-line-length=120 --benchmark &&\
	$(EXECUTOR) pydocstyle

## Run the test suite using pytest
test:
	$(EXECUTOR) pytest

## Release a new version, bumping part=<major|minor|patch>
release:
	$(EXECUTOR) bump2version $$part &&\
	$(EXECUTOR) auto-changelog &&\
	git add CHANGELOG.md &&\
	git commit --amend --no-edit &&\
	echo git push --follow-tags
