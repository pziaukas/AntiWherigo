# AntiWherigo (v0.4.0)



## Introduction 
A tool to reverse-engineer [Wherigo](https://www.wherigo.com) cartridges.

Its purpose is to reveal a hidden source code by employing the following steps:
0. Unpack a `GWC` file into media files and a `LUAC` script.
0. Decompile a `LUAC` script into a plain text `LUA` script.
0. Decrypt obfuscated strings within a `LUA` script (if needed).
0. Combine results into a comprehensive overview.



## Getting Started
The solution is developed primarily on a Unix-like operating system.

### Software dependencies
* [Lua](https://www.lua.org) programming language
* [Python](https://www.python.org) programming language
* [Poetry](https://python-poetry.org) dependency management and packaging tool
* [Java Runtime Environment](https://www.java.com)


### Installation process
Collect, resolve and install required Python dependencies with the following command
```
make environment
```



## Usage

### Command line interface
```
Usage: antiwherigo [OPTIONS] FILENAME

  Reverse-engineer a Wherigo cartridge file.

Options:
  -o, --output PATH  Destination of the output.
  --help             Show this message and exit.
```

### Example
Input:
```
antiwherigo hello_cartridge.gwc
```

Output:
```
Analyzing 'hello_cartridge.gwc' cartridge file.

Start of metadata...
LENGTH: 1000
LATITUDE: -33.123456
LONGITUDE: 151.123456
ALTITUDE: 0.0
DATE: 473657052
ID_SPLASH: -1
ID_ICON: 2
TYPE: TourGuide
PLAYER: AntiWherigo
PLAYER_ID: 123456
CARTRIDGE_NAME: Hello Cartridge
CARTRIDGE_GUID: xxxxxxxx-xxxx-xxxx-xxxx-e04952383010
CARTRIDGE_DESC: Lorem ipsum dolor sit amet, consectetur adipiscing elit
STARTING_LOCATION_DESCRIPTION: Some location
VERSION: 1.5
AUTHOR: Author
COMPANY: 
RECOMMENDED_DEVICE: Windows PPC
COMPLETION_LENGTH: 19
COMPLETION_CODE: GIYDAVCZG42DSMZXHA
...end of metadata.

Unpacking components into '/tmp/hello_cartridge'...
Saved 'metadata.yaml'.
Saved 'hello_cartridge.luac'.
Also found 3 media files:
Saved '0.jpg'.
Saved '1.jpg'.
Saved '2.jpg'.
...done with unpacking.

Decompiling 'hello_cartridge.luac'...
Saved 'hello_cartridge.lua'.
...done with decompiling.

Looking for encrypted content...
Found decryption key '_ABC'.
Made changes to 'hello_cartridge.lua'.
...done with decryption.

All done!
```
