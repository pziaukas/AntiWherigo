"""Global configurations and imports for Anti Wherigo project."""

__version__ = '0.4.0'

from .cli import process
from .byte_reader import ByteFile, ByteReader
from .cartridge import Cartridge

__all__ = [process, ByteFile, ByteReader, Cartridge]
