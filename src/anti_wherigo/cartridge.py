"""Unpack a GWC file into media files and a LUAC script."""
from anti_wherigo import ByteFile, ByteReader
from pathlib import Path

CODE_TYPE = {
    1: 'bmp',
    2: 'png',
    3: 'jpg',
    4: 'gif',
    17: 'wav',
    18: 'mp3',
    19: 'fdl',
    20: 'snd',
    21: 'ogg',
    33: 'swf',
    49: 'txt',
}

TITLE_TYPE = [
    ('LENGTH', 'int'),
    ('LATITUDE', 'double'),
    ('LONGITUDE', 'double'),
    ('ALTITUDE', 'double'),
    ('DATE', 'long'),
    ('ID_SPLASH', 'short'),
    ('ID_ICON', 'short'),
    ('TYPE', 'string'),
    ('PLAYER', 'string'),
    ('PLAYER_ID', 'long'),
    ('CARTRIDGE_NAME', 'string'),
    ('CARTRIDGE_GUID', 'string'),
    ('CARTRIDGE_DESC', 'string'),
    ('STARTING_LOCATION_DESCRIPTION', 'string'),
    ('VERSION', 'string'),
    ('AUTHOR', 'string'),
    ('COMPANY', 'string'),
    ('RECOMMENDED_DEVICE', 'string'),
    ('COMPLETION_LENGTH', 'int'),
    ('COMPLETION_CODE', 'string')
]


class Cartridge:
    """Entity that holds a parsed GWC cartridge.

    Attributes:
        metadata (ByteFile): Header of a cartridge.
        script (ByteFile): Lua (compiled) script of a cartridge.
        medias (List[File]): Media items of a cartridge.

    """

    def __init__(self, filename: Path) -> None:
        """Initialize a Cartridge with the file location."""
        reader = ByteReader(filename)
        if reader.read('short') != int('0a02', 16) or reader.read('string') != 'CART':
            raise TypeError('Cartridge is corrupted or not valid!')

        file_count = reader.read('ushort')
        addresses = [(reader.read('ushort'), reader.read('int'))[1] for _ in range(file_count)]

        self.metadata = {title: reader.read(type_name) for title, type_name in TITLE_TYPE}

        reader.seek(addresses[0])
        size = reader.read('int')
        self.script = ByteFile(f'{filename.stem}.luac', reader.read_bytes(size))

        self.medias = []
        for n, address in enumerate(addresses[1:]):
            reader.seek(address)
            if reader.read('byte'):
                code = reader.read('int')
                size = reader.read('int')

                if code in CODE_TYPE:
                    self.medias.append(ByteFile(f'{n}.{CODE_TYPE[code]}', reader.read_bytes(size)))
                else:
                    print(f'Warning: media code {code} is not of a recognized format!')
