"""Read bytes of a given file."""
import binascii
from dataclasses import dataclass
from pathlib import Path
import struct
from typing import Any

TYPE_FORMAT = {
    'byte': '<b',
    'short': '<h',
    'ushort': '<H',
    'int': '<i',
    'uint': '<I',
    'long': '<q',
    'double': '<d',
}


@dataclass
class ByteFile:
    """Entity that holds its name and content.

    Attributes:
        name: Name of a file.
        content: Raw bytes or string-like content.

    """

    name: str
    content: bytes

    def save(self, output_directory: Path) -> Path:
        """Save file to the given directory, return its full path."""
        filename = Path(output_directory, self.name)
        filename.write_bytes(self.content)
        print(f"Saved '{self.name}'.")
        return filename


class ByteReader:
    """Entity that processes raw bytes of data.

    Attributes:
        stream (BufferedReader): An IO stream of bytes.

    """

    def __init__(self, filename: Path) -> None:
        """Initialize a ByteReader with the file location."""
        self.stream = filename.open('rb')

    def __del__(self) -> None:
        """Close the file stream on disposal."""
        self.stream.close()

    def seek(self, position: int) -> None:
        """Seek a specific location in the stream."""
        self.stream.seek(position, 0)

    def read(self, type_name: str) -> Any:
        """Read content from the stream given a specific format."""
        if type_name == 'string':
            return self.read_string()

        type_format = TYPE_FORMAT[type_name]
        type_size = struct.calcsize(type_format)
        value = self.stream.read(type_size)
        if type_size != len(value):
            raise LookupError
        return struct.unpack(type_format, value)[0]

    def read_int(self) -> int:
        """Read a single byte integer from the stream."""
        byte = self.stream.read(1)
        if len(byte) != 1:
            raise LookupError
        return int(binascii.hexlify(byte), 16)

    def read_string(self) -> str:
        """Read string from the stream."""
        result = ''
        position = self.read_int()
        while position:
            result += chr(position)
            position = self.read_int()
        return result

    def read_bytes(self, size: int) -> bytes:
        """Read a given number of bytes from the stream."""
        return self.stream.read(size)
