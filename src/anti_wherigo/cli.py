"""Interact with the client-facing command line."""
import anti_wherigo as aw
import click
from jinja2 import Template
from pathlib import Path
import re
import sh
import yaml

DECOMPILER_PATH = Path('bin', 'unluac.jar')
DECODER_SCRIPT_PATH = Path('scripts', 'decoder.lua.jj')


@click.command()
@click.argument('filename', type=click.Path(exists=True))
@click.option('--output', '-o', type=click.Path(exists=True), help='Destination of the output.')
def process(filename: str, output: str) -> None:
    """Reverse-engineer a Wherigo cartridge file."""
    gwc_path = Path(filename)
    name = gwc_path.stem
    print(f"Analyzing '{gwc_path.name}' cartridge file.\n")
    cartridge = aw.Cartridge(gwc_path)
    print('Start of metadata...')
    for title, value in cartridge.metadata.items():
        print(f'{title}: {str(value)[:100]}')
    print('...end of metadata.\n')

    output_directory = Path(output) if output else gwc_path.absolute().parent / name
    try:
        output_directory.mkdir()
        print(f"Unpacking components into '{output_directory}'...")
    except FileExistsError:
        print(f'Error: {output_directory} already exists!')
        return
    metadata_name = 'metadata.yaml'
    Path(output_directory, metadata_name).write_text(yaml.dump(cartridge.metadata))
    print(f"Saved '{metadata_name}'.")

    tmp_directory = Path(output_directory, 'tmp')
    tmp_directory.mkdir()
    luac_path = cartridge.script.save(tmp_directory)

    if len(cartridge.medias):
        print(f'Also found {len(cartridge.medias)} media files:')
        media_directory = Path(output_directory, 'media')
        media_directory.mkdir()
        for media in cartridge.medias:
            media.save(media_directory)
    else:
        print('No media files found.')
    print('...done with unpacking.\n')

    print(f"Decompiling '{luac_path.name}'...")
    lua_content = str(sh.java('-jar', DECOMPILER_PATH, luac_path))
    lua_name = f'{name}.lua'
    lua_path_tmp = Path(tmp_directory, lua_name)
    lua_path_tmp.write_text(lua_content)
    print(f"Saved '{lua_name}'.")
    print('...done with decompiling.\n')

    lua_path = Path(output_directory, lua_name)
    print('Looking for encrypted content...')
    if 'dtable' in lua_content:
        match = re.search(r'function (.*?)\(str\).*dtable.*?return.*?end', lua_content, re.DOTALL)
        context = {
            'key': match.group(1),
            'key_function': match.group(0),
            'input_path': lua_path_tmp,
            'output_path': lua_path
        }
        print(f"Found decryption key '{context['key']}'.")
        decoder_path = Path(tmp_directory, 'decoder.lua')
        decoder_template = Template(DECODER_SCRIPT_PATH.read_text())
        decoder_path.write_text(decoder_template.render(**context))
        sh.lua(decoder_path)
        print(f"Made changes to '{lua_name}'.")
    else:
        print('Encrypted content was not found.')
        sh.mv(lua_path_tmp, lua_path)
    print('...done with decryption.\n')

    print('All done!')


if __name__ == '__main__':
    process()
