# Changelog

## v0.4.0 (2020-01-08)

#### New Features

* decrypt obfuscated parts of the LUA script
* decompile LUA (using unluac tool)
* semantic versioning tools
* unpack GWC to LUAC (+media)
* a CLI endpoint
* environment creation routine
#### Fixes

* move versioning tools to dev dependencies
* (ci): add poetry to PATH
#### Performance improvements

* lock the python dependency versions
#### Refactorings

* move unpack functionality to the Cartridge method
#### Others

* gitlab pipeline to test and lint
* (release): 0.4.0
* (release): 0.3.1
* (release): 0.3.0
* (release): 0.2.1
* add licensing
* initial repository outline
